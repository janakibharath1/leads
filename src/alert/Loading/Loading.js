import React from 'react'
import Lottie from 'react-lottie'
import * as loadingMessage from '../LottieFiles/22787-success-loader-with-counter.json'

export default function Loading() {

    const defaultOptions = {
        loop: true,
        autoplay: true, 
        animationData: loadingMessage.default,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
      };
  
    return (
        <div>
        <Lottie 
         options={defaultOptions}
         width='200px'
        />
            
        </div>
    )
}
