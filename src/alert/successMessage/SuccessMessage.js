import React from 'react'
import Lottie from 'react-lottie'
import * as successMessage from '../LottieFiles/5449-success-tick.json'

export default function SuccessMessage() {

    const defaultOptions = {
        loop: true,
        autoplay: true, 
        animationData: successMessage.default,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
      };
  
    return (
        <div >
        <Lottie 
         options={defaultOptions}
         width='200px'
        />
            
        </div>
    )
}
