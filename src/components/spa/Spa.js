import React from 'react'
import { Col, Container, Image, Row } from 'react-bootstrap'
import spa from '../../assets/saloon/massage.svg'
import './spa.css'

export default function Spa() {
    return (
        <Container fluid className='spa_container' >
            <Row className='spa_row' >
               <Col xl={6} lg={6} md={5} sm={12} xs={12} >
                <Image className='spa_image' src={spa} alt="massage" />
                +</Col>
                <Col xl={6} lg={6} md={7} sm={12} xs={12} >
                <h1 className='spa_header' > Massage Parlor </h1>
                <Col xl={12} lg={12} md={7} sm={12} xs={12}  >      
                <span className='spa_text' >
                  Having quality beauty products and professional beauticians can truly grow your beauty salon business.
                  Furthermore,a good website will even make it grow and expand. Eight Five Zero Salon has a simple,
                  minimal yet elegant design for beauty salon. 
                  The site welcomes its visitors with big and stunning images of beautiful women displayed using the image slider. 
                  In addition, the CTAs are in good spots to generate more leads. To increase visitor’s retention to the website,
                  it uses a fixed header so the menu will always be visible and accessible. 
                  Moreover, the trendy parallax effect greatly improves the look of the website which in turn enhances the look and feel of the site.
                </span>
                </Col>
                </Col>
                
            </Row>
        </Container>
    )
}
