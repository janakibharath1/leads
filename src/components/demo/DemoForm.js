import React, { useState } from 'react'
import { Button, Col, Container, Form, InputGroup, Row } from 'react-bootstrap'
import SuccessMessage from '../../alert/successMessage/SuccessMessage';
import SuccessModal from '../SuccessModal/SuccessModal';
import './demoform.css'
import FormApi from './FormApi';
export default function DemoForm() {


    const {handleDemoForm,store, demoForm,validated, handleSubmit,showSuccess} = FormApi();
   
    return (
        <>
        {!showSuccess ? (
             <Container fluid className="demoform_container" >
             <Row>
                 <Col xl={12} lg={12} md={12} sm={12} xs={12} >
                  <h1 className='demoform_contact' >Contact Us</h1>
                  <span className='demoform_text' > Please fill the form for demo.... </span>
                 </Col>
             </Row>
             <Row>
             <Form className='demoform_form' onSubmit={handleSubmit}>
                     <Form.Row>
                         <Form.Group as={Col} md="4" xl="4" lg="4" sm="10" xs="10" controlId="validationCustom01">
                         <Form.Label>First name</Form.Label>
                         <Form.Control
                             name="firstName"
                             required
                             type="text"
                             placeholder="First name"
                             value={demoForm.firstName}
                             onChange={handleDemoForm}
                         />
                         
                         </Form.Group>
                         <Form.Group as={Col} md="4" xl="4" lg="4" sm="10" xs="10" controlId="validationCustom02">
                         <Form.Label>Last name</Form.Label>
                         <Form.Control
                             name="lastName"
                             required
                             type="text"
                             placeholder="Last name"
                             value={demoForm.lastName}
                             onChange={handleDemoForm}
                         />
                         
                         </Form.Group>
                         <Form.Group as={Col} md="4" xl="4" lg="4" sm="10" xs="10" controlId="validationCustom01">
                         <Form.Label>Email</Form.Label>
                         <Form.Control
                             required
                             name="email"
                             type="email"
                             placeholder="Enter your email "
                             value={demoForm.email}
                             onChange={handleDemoForm}
                         />
                         
                         </Form.Group>
                     </Form.Row>
                     <Form.Row>
                     <Form.Group as={Col} md="4" xl="4" lg="4" sm="10" xs="10" controlId="validationCustom03">
                         <Form.Label>Mobile</Form.Label>
                         <Form.Control 
                          type="text" 
                          name="phoneNumber"
                          placeholder="Enter Mobile Number"
                          required 
                          value={demoForm.phoneNumber}
                          onChange={handleDemoForm}
                          />
                        
                         </Form.Group>
                         <Form.Group as={Col} md="4" xl="4" lg="4" sm="10" xs="10" controlId="validationCustom04">
                         <Form.Label>Business Name</Form.Label>
                         <Form.Control
                          type="text"
                          name="businessName" 
                          placeholder="Business Name" 
                          required 
                          value={demoForm.businessName}
                          onChange={handleDemoForm}
                          />
                         
                         </Form.Group>
                         <Form.Group as={Col} md="4" xl="4" lg="4" sm="10" xs="10" controlId="validationCustom03">
                         <Form.Label>No of Store</Form.Label>
                         <Form.Control 
                         type="text" 
                         as="select"
                         name="storeId" 
                         placeholder="No of Store" 
                         required 
                         value={demoForm.id}
                         onChange={handleDemoForm}
                         >
                         <option>Choose</option>
                         { store.map((data)=>(
                             <option key={data.id} >
                              {data.storeName}
                             </option>
                         ))}
                         </Form.Control>
                         </Form.Group>
                         
                     </Form.Row>
                 
                     <Form.Row>
                     <Form.Group as={Col} md="12" xl="12" lg="12" sm="10" xs="10" controlId="validationCustom03">
                         <Form.Label>Message</Form.Label>
                         <Form.Control 
                         as="textarea" 
                         name="message" 
                         placeholder="Enter your message" 
                         required 
                         value={demoForm.message}
                         onChange={handleDemoForm}
                         />
                        
                         </Form.Group>
                     </Form.Row>
                   
                     <Button type="submit">Demo request</Button>
                </Form>
             </Row>
         </Container>

        ) : 
        <Container fluid  className="demoform_container">
            <Row>
             <SuccessModal/>       
            </Row>
        </Container>
        
        }
        </>
       
    )
}
