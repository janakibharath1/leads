import React from 'react'
import {Col, Container, Row} from 'react-bootstrap'
import DemoForm from '../demo/DemoForm'
import Gym from '../gym/Gym'
import Saloon from '../saloon/Saloon'
import Spa from '../spa/Spa'
export default function LandingPage() {
    return (
        <>
        <Container fluid >
        <Row>
            <Saloon/>
        </Row>
        <Row>
            <Spa/>
        </Row>
        <Row>
            <Gym/>
        </Row>
        <Row>
            <DemoForm/>
        </Row>
        </Container>
        </>
    )
}
