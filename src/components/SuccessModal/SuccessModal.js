import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import SuccessMessage from '../../alert/successMessage/SuccessMessage'
import './successModal.css'

export default function SuccessModal() {
    return (
        <>
        <Container>
            <Row>
                <Col >
                <div className='success_messsage' >
                   Submitted Successfully, 
                </div>
                </Col>
            </Row>
            <Row>
                <Col>
                <div className='success_messsage' >
                  support person will get back to you shortly.
                </div>
                </Col>
            </Row>
            <Row className='succcess_modal_tick' >
                <Col>
                <SuccessMessage/>
                </Col>
            </Row>
        </Container>
            
        </>
    )
}
