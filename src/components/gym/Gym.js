import React from 'react'
import { Button, Col, Container, Form, Image, InputGroup, Row } from 'react-bootstrap'
import './gym.css'
import beauty from '../../assets/saloon/spa.svg'

export default function Gym() {

    return (
        <Container fluid  className='gym_container' >
           <Row className='gym_row' >
               <Col xl={6} lg={6} lg={6} md={6} sm={12} xs={12} >
                <Image className='gym_image' src={beauty} alt="Gym" />
                +</Col>
                <Col xl={6} lg={6} lg={6} md={6} sm={12} xs={12} >
                <h1 className='gym_header' > Facial </h1>
                <Col xl={12} >      
                <span className='gym_text' >
                  Having quality beauty products and professional beauticians can truly grow your beauty salon business.
                  Furthermore,a good website will even make it grow and expand. Eight Five Zero Salon has a simple,
                  minimal yet elegant design for beauty salon. 
                  The site welcomes its visitors with big and stunning images of beautiful women displayed using the image slider. 
                  In addition, the CTAs are in good spots to generate more leads. To increase visitor’s retention to the website,
                  it uses a fixed header so the menu will always be visible and accessible. 
                  Moreover, the trendy parallax effect greatly improves the look of the website which in turn enhances the look and feel of the site.
                </span>
                </Col>
                </Col>
                
            </Row>
        </Container>
    )
}
