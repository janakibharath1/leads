import React, { useEffect, useState } from 'react'
import axios from 'axios'
import {useHistory} from 'react-router-dom'

function FormApi() {
  
 const [demoForm, setDemoForm] = useState({
        firstName: '',
        lastName: '',
        email: '',
        storeId: '',
        phoneNumber: '',
        businessName: '',
        message: ''
    });
    console.log("Hell", `${demoForm.firstName} ${demoForm.lastName}`)
 
    const history = useHistory();

//  const[store, setStore] = useState([]);
 const [validated, setValidated] = useState(false);
 const[showSuccess, setShowSuccess] = useState(false);
 const[showLoading, setShowLoading] = useState(false);

 const store = [
    { id: 1, storeName: "01", count: 1 },
    { id: 2, storeName: "02", count: 2 },
    { id: 3, storeName: "03", count: 3 },
    { id: 4, storeName: "04", count: 4 },
    { id: 5, storeName: "05", count: 5 },
 ]

    const handleDemoForm = (e) => {
        const { name, value } = e.target;

        setDemoForm({
        ...demoForm,
        [name]: value,
        });
    }

    const demoFormSubmit = () =>{
       
        const formData = new FormData();

        formData.append("ContactName",  `${demoForm.firstName} ${demoForm.lastName}`);
        //formData.append("lastName", demoForm.lastName);
        formData.append("EmailId", demoForm.email);        
        formData.append("ContactNumber", demoForm.phoneNumber);
        formData.append("BusinessName", demoForm.businessName);
        formData.append("NumberOfLicenses", demoForm.storeId);
        formData.append("Message", demoForm.message);
    
        axios({
          method: "post",
          url:
            "enquiries",
          data: formData,
        })
          .then(function (response) {
            console.log(response);
            // clearForm();
            handleShowSuccess();
            clearForm();
            setShowLoading(false)
            setInterval(() => {
              setShowSuccess(false)
              history.push('/')
            }, 5000);
          })
          .catch(function (response) {
            //handle error
            alert(response);
            setShowLoading(false)
            console.log(response);
          });
    }

    // useEffect(()=>{
    //   let token = localStorage.getItem("user");
    //   const AuthStr = "Bearer ".concat(token);
    //   axios({
    //     method: "get",
    //     url: "https://tlgqz4quj3.execute-api.us-east-1.amazonaws.com/Prod/stores",  
    //     headers: { Authorization: AuthStr },
    //   })
    //     .then(function (response) {
    //       console.log(response);
    //       setStore(response.data)
    //     })
    //     .catch(function (response) {
    //       //handle error
    //       alert(response);
    //       console.log(response);
    //     });
    // }, [])

    const clearForm = () =>{
        setDemoForm({
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            businessName: '',
            storeId:  '',
            message: ''
            });
    }

    const handleShowSuccess = () =>{
      setShowSuccess(true)
  }

    const handleSubmit = (event) => {
        event.preventDefault();
       setValidated(true);
        demoFormSubmit();
        setShowLoading(true)
        console.log("DemoData", demoForm)
    };

    return {handleDemoForm,handleSubmit,validated,store,showLoading, demoForm, showSuccess}
        
    
}

export default FormApi
