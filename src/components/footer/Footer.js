import React from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'
import './footer.css'

export default function Footer() {
    return (
        <Container fluid className='footer_container' >
         <Navbar className='footer_nav' bg="dark" variant="dark" >
            <Navbar.Brand href="#home">
              All rights are reserved...
            </Navbar.Brand>
            <Nav>
                <Nav.Link>Contact Us</Nav.Link>
                <Nav.Link>Careers</Nav.Link>

            </Nav>
        </Navbar>
        </Container>
    )
}
