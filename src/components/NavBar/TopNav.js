import React, { useRef, useState } from 'react'
import { Button, DropdownButton, Dropdown, Form, FormControl, Nav, Navbar, NavDropdown, Modal, Overlay } from 'react-bootstrap'
import './topnav.css'
import userLogo from '../../assets/images/SD.svg'
import bellLogo from '../../assets/images/notification.svg'
import DemoFormModal from '../FormModal/DemoFormModal'
import {Link} from 'react-router-dom'

export default function TopNav(props) {

    const [show, setShow] = useState(false);
    const target = useRef(null);

    const openModal = () =>{
        setShow(true);
    }

    const closeModal = () =>{
      setShow(false)
    }

    return (
        <>
           <Navbar fixed="top" className= "top_nav_navbar">
                <Navbar.Brand as={Link} to='/' className='top_nav_brand' >Purple</Navbar.Brand>
                <Nav className="mr-auto top_nav "  >
                <Nav.Link as={Link} to='/' >Home</Nav.Link>
                <Nav.Link as={Link} to='/products' >Products</Nav.Link>
                <Nav.Link as={Link} to='/services' >Services</Nav.Link>
                <Button ref={target} onClick={openModal} className='demo_btn' >Demo request</Button>
                </Nav>
                <Form className='top_nav_search' inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2 " />
                </Form>
                <img src={bellLogo} className="top_nav_belllogo" />
                {/* <img src={userLogo} className="top_nav_userlogo" /> */}
                <NavDropdown
                    className='nav_dropdown'
                    menuAlign="left"
                    title="Profile"
                    id="dropdown-menu-align-left"
                  >
                    <NavDropdown.Item eventKey="4.1">Action</NavDropdown.Item>
                    <NavDropdown.Item eventKey="4.2">Another action</NavDropdown.Item>
                    <NavDropdown.Item eventKey="4.3">Something else here</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item eventKey="4.4">Separated link</NavDropdown.Item>
                </NavDropdown>
            </Navbar> 
             
            <Overlay target={target.current} show={show} placement="bottom">
             {({ placement, arrowProps, show: _show, popper, ...props }) => (
          <div
            {...props}
            style={{
              width: '550px',
              height: '500px',
              marginTop:'20px',
              marginLeft:'280px',
              backgroundColor: 'rgba(255, 100, 100, 0.85)',
              padding: '40px 40px', 
              borderRadius: 20,
              ...props.style,
            }}
          >
            <DemoFormModal close={closeModal} />
           
          </div>
        )}
      </Overlay>
          
           
        </>
    )
}
