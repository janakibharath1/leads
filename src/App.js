import logo from './logo.svg';
import './App.css';
import {HashRouter as Router, Route, Switch, useHistory} from 'react-router-dom'
import TopNav from './components/NavBar/TopNav';
import 'bootstrap/dist/css/bootstrap.min.css';
import LandingPage from './components/landingPage/LandingPage';
import Footer from './components/footer/Footer';
import Products from './components/Products/Products';
import Services from './components/Services/Services';
import Saloon from './components/saloon/Saloon';
import Gym from './components/gym/Gym';
import Spa from './components/spa/Spa';
import axios from 'axios';

function App() {
  let history = useHistory();
  axios.defaults.baseURL=`https://6izabx2m8d.execute-api.us-east-1.amazonaws.com/Prod/`;
  let AUTH_TOKEN = localStorage.getItem("user");
  axios.defaults.headers.common['Authorization'] ="Bearer "+ AUTH_TOKEN;
  return (
   <>
   <Router history={history} >
     <TopNav/>
     <Switch>
       <Route path='/' exact component={LandingPage} />
       <Route path='/products' component={Spa} />
       <Route path='/services' component={Gym} />
     </Switch>
      <Footer/>
   </Router>
   </>
  );
}

export default App;
