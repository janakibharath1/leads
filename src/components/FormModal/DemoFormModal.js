import React, { useState } from 'react'
import './formModal.css'
import { Button, Col, Container, Form, InputGroup, Modal, Row } from 'react-bootstrap'
import FormApi from '../demo/FormApi';
import SuccessModal from '../SuccessModal/SuccessModal';
import Loading from '../../alert/Loading/Loading';

export default function DemoFormModal(props) {
   
    const {handleDemoForm,store,showLoading, demoForm,validated, handleSubmit, showSuccess} = FormApi();
    
    return (
        <>
        
        {!showSuccess ? (

            <Container fluid className="demoform_modal_container" >
                    
            <Row>
                <Col>
                    <h1 className='demo_form_modal_head' >
                    Fill the form for Demo request
                    </h1>
                
                </Col>
            </Row>
            <Row>
            <Form className='demoform_modal_form' onSubmit={handleSubmit}>
                    <Form.Row>
                        <Form.Group as={Col} md="6" controlId="validationCustom01">
                        <Form.Label>First name</Form.Label>
                        <Form.Control
                            name="firstName"
                            required
                            type="text"
                            placeholder="First name"
                            value={demoForm.firstName}
                            onChange={handleDemoForm}
                        />
                        
                        </Form.Group>
                        <Form.Group as={Col} md="6" controlId="validationCustom02">
                        <Form.Label>Last name</Form.Label>
                        <Form.Control
                            name="lastName"
                            required
                            type="text"
                            placeholder="Last name"
                            value={demoForm.lastName}
                            onChange={handleDemoForm}
                        />
                        
                        </Form.Group>
                    
                    </Form.Row>
                    <Form.Row>
                    <Form.Group as={Col} md="6" controlId="validationCustom01">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            required
                            name="email"
                            type="email"
                            placeholder="Enter your email "
                            value={demoForm.email}
                            onChange={handleDemoForm}
                        />
                        
                        </Form.Group>

                        <Form.Group as={Col} md="6" controlId="validationCustom03">
                        <Form.Label>Mobile</Form.Label>
                        <Form.Control 
                        type="text" 
                        name="phoneNumber"
                        placeholder="Enter Mobile Number"
                        required 
                        value={demoForm.phoneNumber}
                        onChange={handleDemoForm}
                        />
                    
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} md="6" controlId="validationCustom04">
                        <Form.Label>Business Name</Form.Label>
                        <Form.Control
                        type="text"
                        name="businessName" 
                        placeholder="Business Name" 
                        required 
                        value={demoForm.businessName}
                        onChange={handleDemoForm}
                        />
                        
                        </Form.Group>
                        <Form.Group as={Col} md="6" controlId="validationCustom03">
                        <Form.Label>No of Store</Form.Label>
                        <Form.Control 
                        type="text" 
                        as="select"
                        name="storeId" 
                        placeholder="No of Store" 
                        required 
                        value={demoForm.id}
                        onChange={handleDemoForm}
                        >
                        <option>Choose</option>
                        { store.map((data)=>(
                            <option key={data.id} >
                            {data.storeName}
                            </option>
                        ))}
                        </Form.Control>
                        </Form.Group>
                        
                    </Form.Row>
                
                    <Form.Row>
                    <Form.Group as={Col} md="12" controlId="validationCustom03">
                        <Form.Label>Message</Form.Label>
                        <Form.Control 
                        as="textarea" 
                        name="message" 
                        placeholder="Enter your message" 
                        required 
                        value={demoForm.message}
                        onChange={handleDemoForm}
                        />
                    
                        </Form.Group>
                    </Form.Row>
                    <Form.Row xl={8} >
                        <Col xl={4} lg={4} md={4} >
                        <Button className='demo_form_modal_btn_cancle' variant="success" type="submit">Demo request</Button>       
                        </Col>
                        <Col xl={6} lg={6} md={6} >
                        <Button onClick={props.close} className='demo_form_modal_btn_cancle' variant="secondary" type="button">Cancel</Button>    
                        </Col >
                    </Form.Row>

            </Form>
            </Row>
            {
            showLoading ? <Loading/> : " "
        }

            </Container>
        ) : (

            <Container fluid className="demoform_modal_container" >
                <Row>
                    <Col>
                      <SuccessModal/>    
                    </Col>
               </Row>
            </Container>
        )
        
    }
        </>

        
    )
}
